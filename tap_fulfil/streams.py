"""Stream type classes for tap-fulfil."""

from typing import Any, Optional

import requests
from singer_sdk import typing as th

from tap_fulfil.client import FulFilStream, FulFilStreamChildStream

rec_blurb = th.ObjectType(
            th.Property("description", th.CustomType({"type": ["string", "integer", "object"]})),
            th.Property("id", th.IntegerType),
            th.Property("subtitle", th.ArrayType(th.CustomType({"type": ["array", "object", "string", "integer"]}))),
            th.Property("title", th.StringType),
        )

class ProductsStream(FulFilStream):
    name = "products"
    path = "/product.product"
    primary_keys = ["id"]
    replication_filter = {"filter": "updated_at_min", "stream": "product_individual"}

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("rec_name", th.StringType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "product_id": record["id"],
        }


class ProductStream(FulFilStreamChildStream):
    name = "product_individual"
    path = "/product.product/{product_id}"
    primary_keys = ["product_id"]
    parent_stream_type = ProductsStream
    replication_key = "write_date"

    schema = th.PropertiesList(
        th.Property("abc_classification", th.StringType),
        th.Property("account_category", th.IntegerType),
        th.Property("account_cogs_used", th.IntegerType),
        th.Property("account_expense_used", th.IntegerType),
        th.Property("account_revenue_used", th.IntegerType),
        th.Property("account_stock_used", th.IntegerType),
        th.Property("active", th.BooleanType),
        th.Property("allow_open_amount", th.BooleanType),
        th.Property("asin", th.StringType),
        th.Property("attachments", th.CustomType({"type": ["array", "string"]})),
        th.Property("attribute_set", th.IntegerType),
        th.Property("attributes", th.ArrayType(th.IntegerType)),
        th.Property("attributes_json", th.ArrayType(
            th.ObjectType(
                th.Property("attribute_display_name", th.StringType),
                th.Property("attribute_id", th.IntegerType),
                th.Property("attribute_name", th.StringType),
                th.Property("attribute_set_id", th.IntegerType),
                th.Property("attribute_type", th.StringType),
                th.Property("id", th.IntegerType),
                th.Property("product_id", th.IntegerType),
                th.Property("value", th.StringType),
                th.Property("value_boolean", th.BooleanType),
                th.Property("value_char", th.StringType),
                th.Property("value_date", th.StringType),
                th.Property("value_datetime", th.StringType),
                th.Property("value_float", th.NumberType),
                th.Property("value_integer", th.IntegerType),
                th.Property("value_numeric", th.NumberType),
                th.Property("value_selection", th.CustomType({"type": ["string", "number"]}))
            )
        )),
        th.Property("average_daily_consumed", th.NumberType),
        th.Property("average_daily_sales", th.NumberType),
        th.Property("average_monthly_consumed", th.NumberType),
        th.Property("average_monthly_sales", th.NumberType),
        th.Property("average_price", th.NumberType),
        th.Property("boms", th.CustomType({"type": ["array", "string"]})),
        th.Property("box_type", th.CustomType({"type": ["string", "integer"]})),
        th.Property("brand", th.IntegerType),
        th.Property("built_on_the_fly_bom", th.CustomType({"type": ["string", "integer"]})),
        th.Property("channel_listings", th.ArrayType(th.IntegerType)),
        th.Property("code", th.StringType),
        th.Property("codes", th.ArrayType(th.CustomType({"type": ["string", "integer"]}))),
        th.Property("company_currency_symbol", th.StringType),
        th.Property("consumable", th.BooleanType),
        th.Property("cost_price", th.NumberType),
        th.Property("cost_price_method", th.StringType),
        th.Property("cost_price_uom", th.NumberType),
        th.Property("cost_prices", th.ArrayType(th.IntegerType)),
        th.Property("cost_value", th.NumberType),
        th.Property("country_of_origin", th.CustomType({"type": [ "string", "integer"]})),
        th.Property("create_date", th.DateTimeType),
        th.Property("create_uid", th.IntegerType),
        th.Property("cross_sells",  th.ArrayType(th.CustomType({"type": [ "string", "integer"]}))),
        th.Property("customs_description", th.CustomType({"type": [ "string", "object"]})),
        th.Property("customs_description_used", th.StringType),
        th.Property("customs_value", th.CustomType({"type": [ "string", "integer"]})),
        th.Property("customs_value_used", th.NumberType),
        th.Property("days_of_inventory_left", th.IntegerType),
        th.Property("default_image", th.IntegerType),
        th.Property("default_uom", th.IntegerType),
        th.Property("default_uom_category", th.IntegerType),
        th.Property("default_uom_digits", th.IntegerType),
        th.Property("description", th.CustomType({"type": ["string","integer"]})),
        th.Property("dimensions_uom", th.CustomType({"type": ["string","integer"]})),
        th.Property("discontinued_date", th.CustomType({"type": ["string","integer"]})),
        th.Property("ean", th.CustomType({"type": ["string", "integer"]})),
        th.Property("fnsku", th.CustomType({"type": ["string","integer"]})),
        th.Property("forecast_quantity", th.NumberType),
        th.Property("fulfil_strategy", th.StringType),
        th.Property("gc_max", th.CustomType({"type": ["string","integer"]})),
        th.Property("gc_min", th.CustomType({"type": ["string","integer"]})),
        th.Property("gift_card_delivery_mode", th.StringType),
        th.Property("gift_card_prices", th.ArrayType(th.CustomType({"type": ["string", "integer"]}))),
        th.Property("google_product_category", th.CustomType({"type": ["string", "integer"]})),
        th.Property("gross_margin", th.NumberType),
        th.Property("gross_profit", th.NumberType),
        th.Property("gtin", th.CustomType({"type": ["string","integer"]})),
        th.Property("height", th.CustomType({"type": ["string","integer", "number"]})),
        th.Property("hs_code", th.CustomType({"type": ["string","integer"]})),
        th.Property("id", th.IntegerType),
        th.Property("images", th.ArrayType(th.IntegerType)),
        th.Property("is_fresh_import", th.BooleanType),
        th.Property("is_gift_card", th.BooleanType),
        th.Property("isbn", th.StringType),
        th.Property("landed_cost", th.BooleanType),
        th.Property("lead_time", th.NumberType),
        th.Property("lead_time_in_days", th.IntegerType),
        th.Property("lead_times", th.ArrayType(th.StringType)),
        th.Property("length", th.CustomType({"type": ["string","integer", "number"]})),
        th.Property("list_price", th.NumberType),
        th.Property("list_price_uom", th.NumberType),
        th.Property("list_prices", th.ArrayType(th.IntegerType)),
        th.Property("long_description", th.CustomType({"type": ["string", "integer"]})),
        th.Property("lot_numbers", th.ArrayType(th.IntegerType)),
        th.Property("lot_required", th.ArrayType(th.IntegerType)),
        th.Property("max_daily_sales", th.NumberType),
        th.Property("media", th.ArrayType(th.IntegerType)),
        th.Property("media_json", th.ArrayType(
            th.ObjectType(
                th.Property("id", th.IntegerType),
                th.Property("mimetype", th.StringType),
                th.Property("name", th.StringType),
                th.Property("product", th.IntegerType),
                th.Property("sequence", th.IntegerType),
                th.Property("template", th.StringType),
                th.Property("url", th.StringType),
            )
        )),
        th.Property("messages", th.ArrayType(th.StringType)),
        th.Property("metadata", th.CustomType({"type": ["object", "string"]})),
        th.Property("multiple_warehouses", th.BooleanType),
        th.Property("name", th.StringType),
        th.Property("next_shipping_date", th.CustomType({"type": ["string","integer", "array"]})),
        th.Property("nodes", th.CustomType({"type": ["array", "string"]})),
        th.Property("option_set", th.CustomType({"type": ["string","integer", "boolean"]})),
        th.Property("order_points", th.ArrayType(th.CustomType({"type": ["string","integer"]}))),
        th.Property("price_list_lines", th.CustomType({"type": ["array", "string"]})),
        th.Property("private_notes", th.CustomType({"type": ["array", "string"]})),
        th.Property("producible", th.BooleanType),
        th.Property("product_suppliers", th.ArrayType(th.IntegerType)),
        th.Property("production_order_point", th.CustomType({"type": ["string","integer"]})),
        th.Property("public_notes", th.ArrayType(th.CustomType({"type": ["string","integer"]}))),
        th.Property("purchasable", th.BooleanType),
        th.Property("purchase_order_point", th.CustomType({"type": ["string","integer"]})),
        th.Property("purchase_price_uom", th.NumberType),
        th.Property("purchase_uom", th.IntegerType),
        th.Property("quantity", th.NumberType),
        th.Property("quantity_assigned", th.NumberType),
        th.Property("quantity_available", th.NumberType),
        th.Property("quantity_breakup", th.CustomType({"type": ["object", "string"]})),
        th.Property("quantity_buildable", th.NumberType),
        th.Property("quantity_inbound", th.NumberType),
        th.Property("quantity_on_confirmed_purchase_orders", th.NumberType),
        th.Property("quantity_on_hand", th.NumberType),
        th.Property("quantity_outbound", th.NumberType),
        th.Property("quantity_reserved", th.NumberType),
        th.Property("quantity_returned", th.IntegerType),
        th.Property("quantity_returns_to_be_received", th.NumberType),
        th.Property("quantity_sold", th.NumberType),
        th.Property("quantity_waiting_consumption", th.NumberType),
        th.Property("quantity_wip", th.NumberType),
        th.Property("rec_blurb", rec_blurb),
        th.Property("rec_name", th.StringType),
        th.Property("revenue", th.NumberType),
        th.Property("safety_stock_days", th.IntegerType),
        th.Property("salable", th.BooleanType),
        th.Property("sale_order_count", th.IntegerType),
        th.Property("sale_price_uom", th.NumberType),
        th.Property("sale_return_count", th.IntegerType),
        th.Property("sale_uom", th.IntegerType),
        th.Property("scan_required", th.BooleanType),
        th.Property("sequence", th.IntegerType),
        th.Property("ship_from_stock_if_available", th.BooleanType),
        th.Property("supply_on_sale", th.BooleanType),
        th.Property("template", th.IntegerType),
        th.Property("tsv", th.CustomType({"type": ["string","integer"]})),
        th.Property("type", th.StringType),
        th.Property("up_sells", th.ArrayType(th.StringType)),
        th.Property("upc", th.CustomType({"type": ["string","integer"]})),
        th.Property("use_list_price_as_customs_value", th.BooleanType),
        th.Property("use_name_as_customs_description", th.BooleanType),
        th.Property("use_template_description", th.BooleanType),
        th.Property("variant_name", th.StringType),
        th.Property("volume", th.CustomType({"type": ["string", "integer", "number"]})),
        th.Property("warehouse_locations", th.ArrayType(th.IntegerType)),
        th.Property("warehouse_quantities", th.CustomType({"type": ["object", "string"]})),
        th.Property("weight", th.NumberType),
        th.Property("weight_digits", th.IntegerType),
        th.Property("weight_uom", th.IntegerType),
        th.Property("width", th.CustomType({"type": ["string","integer", "number"]})),
        th.Property("wishlists", th.ArrayType(th.StringType)),
        th.Property("write_date", th.DateTimeType),
        th.Property("write_uid", th.IntegerType)
    ).to_dict()

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        return


class ProductSuppliersStream(FulFilStream):
    name = "product_suppliers"
    path = "/purchase.product_supplier"
    primary_keys = ["id"]
    replication_filter = {"filter": "updated_at_min", "stream": "product_suppliers_individual"}

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("rec_name", th.StringType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "product_supplier_id": record["id"],
        }


class ProductSupplierStream(FulFilStreamChildStream):
    name = "product_suppliers_individual"
    path = "/purchase.product_supplier/{product_supplier_id}"
    primary_keys = ["id"]
    parent_stream_type = ProductSuppliersStream
    replication_key = "write_date"

    schema = th.PropertiesList(
        th.Property("active", th.BooleanType),
        th.Property("attachments", th.CustomType({"type": ["array", "string"]})),
        th.Property("code", th.StringType),
        th.Property("company", th.IntegerType),
        th.Property("create_date", th.DateTimeType),
        th.Property("create_uid", th.IntegerType),
        th.Property("currency", th.IntegerType),
        th.Property("default_unit_digits", th.IntegerType),
        th.Property("draft_purchase_line", th.BooleanType),
        th.Property("drop_shipment", th.BooleanType),
        th.Property("forecast_quantity_on_supply_date", th.NumberType),
        th.Property("forecast_quantity_on_supply_date_calculation",
            th.ArrayType(
                th.ObjectType(
                    th.Property("name", th.StringType),
                    th.Property("value", th.StringType)
                )
            )
        ),
        th.Property("id", th.IntegerType),
        th.Property("lead_time", th.NumberType),
        th.Property("lead_time_in_days", th.IntegerType),
        th.Property("max_unit_price", th.NumberType),
        th.Property("messages", th.ArrayType(th.StringType)),
        th.Property("metadata", th.CustomType({"type": ["object", "string"]})),
        th.Property("metafields", th.ArrayType(th.StringType)),
        th.Property("min_unit_price", th.NumberType),
        th.Property("moq", th.NumberType),
        th.Property("name", th.StringType),
        th.Property("party", th.IntegerType),
        th.Property("prices", th.ArrayType(th.IntegerType)),
        th.Property("private_notes", th.CustomType({"type": ["array", "string"]})),
        th.Property("product", th.IntegerType),
        th.Property("public_notes", th.ArrayType(th.StringType)),
        th.Property("purchase_unit_digits", th.IntegerType),
        th.Property("purchase_uom_symbol", th.StringType),
        th.Property("quantity_available", th.NumberType),
        th.Property("quantity_available_levels", th.ArrayType(th.IntegerType)),
        th.Property("quantity_available_updated", th.IntegerType),
        th.Property("quantity_tracking", th.IntegerType),
        th.Property("rec_blurb", rec_blurb),
        th.Property("rec_name", th.StringType),
        th.Property("sequence", th.IntegerType),
        th.Property("suggested_quantity", th.NumberType),
        th.Property("suggested_quantity_formula", th.StringType),
        th.Property("suggested_quantity_purchase_uom", th.NumberType),
        th.Property("write_date", th.DateTimeType),
        th.Property("write_uid", th.IntegerType)
    ).to_dict()

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        return


class StockLocationsStream(FulFilStream):
    name = "stock_locations"
    path = "/stock.location"
    primary_keys = ["id"]
    replication_filter = {"filter": "updated_at_min", "stream": "stock_location"}

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("rec_name", th.StringType)
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "stock_location_id": record["id"],
        }

class StockLocationStream(FulFilStreamChildStream):
    name = "stock_location"
    path = "/stock.location/{stock_location_id}"
    primary_keys = ["stock_location_id"]
    parent_stream_type = StockLocationsStream
    replication_key = "write_date"

    schema = th.PropertiesList(
        th.Property("active", th.BooleanType),
        th.Property("address", th.CustomType({"type": ["string", "object", "integer"]})),
        th.Property("attachments", th.CustomType({"type": ["array", "string"]})),
        th.Property("bin_transfer_max_lines", th.IntegerType),
        th.Property("bin_transfer_max_quantity", th.IntegerType),
        th.Property("childs", th.ArrayType(th.IntegerType)),
        th.Property("code", th.CustomType({"type": ["string", "integer"]})),
        th.Property("company", th.CustomType({"type": ["string", "integer"]})),
        th.Property("company_name_on_shipping_label", th.StringType),
        th.Property("cost_value", th.CustomType({"type": ["string", "integer"]})),
        th.Property("create_date", th.DateTimeType),
        th.Property("create_uid", th.IntegerType),
        th.Property("credentials", th.StringType),
        th.Property("customer_return_location", th.CustomType({"type": ["string", "object", "integer"]})),
        th.Property("dashboard_link", th.StringType),
        th.Property("dhl_ecommerce_facility_code", th.StringType),
        th.Property("dhl_ecommerce_pickup_number", th.StringType),
        th.Property("enable_production_requests", th.CustomType({"type": ["string", "object", "boolean"]})),
        th.Property("encrypted_credentials", th.StringType),
        th.Property("explode_built_on_the_fly", th.BooleanType),
        th.Property("flat_childs", th.BooleanType),
        th.Property("forecast_quantity", th.IntegerType),
        th.Property("holidays", th.ArrayType(th.IntegerType)),
        th.Property("id", th.IntegerType),
        th.Property("input_location", th.CustomType({"type": ["string", "integer"]})),
        th.Property("integration", th.StringType),
        th.Property("jwt_key_id", th.StringType),
        th.Property("left", th.IntegerType),
        th.Property("location_name_format", th.IntegerType),
        th.Property("mark_shipment_done_on_label", th.BooleanType),
        th.Property("messages", th.ArrayType(th.StringType)),
        th.Property("metadata", th.CustomType({"type": ["object", "string"]})),
        th.Property("metafields", th.ArrayType(th.StringType)),
        th.Property("name", th.StringType),
        th.Property("output_location", th.CustomType({"type": ["string", "integer"]})),
        th.Property("packing_slip_template", th.StringType),
        th.Property("parent", th.IntegerType),
        th.Property("picking_location", th.CustomType({"type": ["string", "integer"]})),
        th.Property("private_notes", th.CustomType({"type": ["array", "string"]})),
        th.Property("production_allocation_future_days", th.IntegerType),
        th.Property("production_allocation_method", th.StringType),
        th.Property("production_location", th.CustomType({"type": ["string", "integer"]})),
        th.Property("production_staging_storage_location", th.CustomType({"type": ["string", "integer"]})),
        th.Property("provisioning_location", th.CustomType({"type": ["string", "integer"]})),
        th.Property("public_notes", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("quantity", th.CustomType({"type": ["string", "integer"]})),
        th.Property("quantity_available", th.CustomType({"type": ["string", "integer", "number"]})),
        th.Property("quantity_on_hand", th.CustomType({"type": ["string", "integer"]})),
        th.Property("rec_blurb", rec_blurb),
        th.Property("rec_name", th.StringType),
        th.Property("receive_and_shelve_in_single_step", th.BooleanType),
        th.Property("replenishment_warehouse", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("return_address", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("right", th.IntegerType),
        th.Property("sequence", th.IntegerType),
        th.Property("shipment_allocation_future_days", th.IntegerType),
        th.Property("shipment_allocation_method", th.StringType),
        th.Property("storage_location", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("supplier_return_location", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("supplier_shelving_location", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("tpl", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("type", th.StringType),
        th.Property("warehouse", th.IntegerType),
        th.Property("warehouse_3pl_schedules", th.ArrayType( th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("warehouse_type", th.StringType),
        th.Property("workflow_configuration", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("write_date", th.DateTimeType),
        th.Property("write_uid", th.IntegerType),
    ).to_dict()

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        return 


class SalesOrders(FulFilStream):
    name = "sales_orders"
    path = "/sale.sale"
    primary_keys = ["id"]
    replication_filter = {"filter": "updated_at_min", "stream": "sales_order_individual"}

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("rec_name", th.StringType)
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "sales_order_id": record["id"],
        }


class SalesOrder(FulFilStreamChildStream):
    name = "sales_order_individual"
    path = "/sale.sale/{sales_order_id}"
    primary_keys = ["sales_order_id"]
    parent_stream_type = SalesOrders
    replication_key = "write_date"

    schema = th.PropertiesList(
        th.Property("access_token", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("acknowledged_at", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("acknowledgement_status", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("amount_invoiced", th.NumberType),
        th.Property("attachments", th.CustomType({"type": ["array", "string"]})),
        th.Property("available_carrier_services", th.ArrayType(th.IntegerType)),
        th.Property("carrier", th.IntegerType),
        th.Property("carrier_cost_method", th.StringType),
        th.Property("carrier_service", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("channel", th.IntegerType),
        th.Property("channel_identifier", th.StringType),
        th.Property("channel_tax_amount", th.NumberType),
        th.Property("channel_type", th.StringType),
        th.Property("comment", th.StringType),
        th.Property("company", th.IntegerType),
        th.Property("company_currency_digits", th.IntegerType),
        th.Property("confirmation_time", th.DateTimeType),
        th.Property("create_date", th.DateTimeType),
        th.Property("create_uid", th.IntegerType),
        th.Property("credit", th.NumberType),
        th.Property("currency", th.IntegerType),
        th.Property("currency_digits", th.IntegerType),
        th.Property("currency_symbol", th.StringType),
        th.Property("description", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("drop_location", th.IntegerType),
        th.Property("drop_shipments", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("dropship_order_reference", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("edi_attributes", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("exceptions", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("external_url", th.StringType),
        th.Property("gateway_transactions", th.ArrayType(th.IntegerType)),
        th.Property("gift_card_method", th.StringType),
        th.Property("gift_message", th.CustomType({"type": ["array", "string"]})),
        th.Property("guest_access_code", th.StringType),
        th.Property("has_channel_exception", th.BooleanType),
        th.Property("has_line_with_discount", th.BooleanType),
        th.Property("has_return", th.BooleanType),
        th.Property("id", th.IntegerType),
        th.Property("invoice_address", th.IntegerType),
        th.Property("invoice_method", th.StringType),
        th.Property("invoice_state", th.StringType),
        th.Property("invoices", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("invoices_ignored", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("invoices_recreated", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("is_cart", th.BooleanType),
        th.Property("is_international_shipping", th.BooleanType),
        th.Property("is_recently_active", th.BooleanType),
        th.Property("last_modification", th.DateTimeType),        
        th.Property("lines", th.ArrayType(th.IntegerType)),
        th.Property("messages", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("metadata", th.CustomType({"type": ["object", "string"]})),      
        th.Property("metafields", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("moves", th.ArrayType(th.IntegerType)),
        th.Property("number", th.StringType),
        th.Property("party", th.IntegerType),
        th.Property("party_lang", th.StringType),
        th.Property("payment_authorize_on", th.StringType),
        th.Property("payment_authorized", th.NumberType),
        th.Property("payment_available", th.NumberType),
        th.Property("payment_capture_on", th.StringType),
        th.Property("payment_captured", th.NumberType),
        th.Property("payment_collected", th.NumberType),
        th.Property("payment_processing_state", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("payment_term", th.IntegerType),
        th.Property("payment_total", th.NumberType),
        th.Property("payments", th.ArrayType(th.IntegerType)),
        th.Property("price_list", th.IntegerType),
        th.Property("priority", th.StringType),
        th.Property("private_notes", th.CustomType({"type": ["array", "string"]})),
        th.Property("promotion", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("public_notes", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("rec_blurb", rec_blurb),
        th.Property("rec_name", th.StringType),
        th.Property("reference", th.StringType),
        th.Property("requested_shipping_service", th.StringType),
        th.Property("sale_date", th.DateType),
        th.Property("shipment_address", th.IntegerType),
        th.Property("shipment_amount", th.NumberType),
        th.Property("shipment_amount_invoiced", th.NumberType),
        th.Property("shipment_cost_markup", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("shipment_cost_method", th.StringType),
        th.Property("shipment_method", th.StringType),
        th.Property("shipment_party", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("shipment_returns", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("shipment_state", th.StringType),
        th.Property("shipments", th.ArrayType(th.IntegerType)),
        th.Property("shipping_cost_computation", th.StringType),
        th.Property("shipping_end_date", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("shipping_instructions", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("shipping_start_date", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("sorted_payments", th.ArrayType(th.IntegerType)),
        th.Property("state", th.StringType), 
        th.Property("tax_amount", th.NumberType),
        th.Property("tax_amount_cache", th.NumberType),
        th.Property("tax_amount_cpny_ccy_cache", th.NumberType),
        th.Property("total_amount", th.NumberType),
        th.Property("total_amount_cache", th.NumberType),
        th.Property("total_amount_cpny_ccy_cache", th.NumberType),
        th.Property("total_quantity", th.NumberType),
        th.Property("total_quantity_uom", th.StringType),
        th.Property("total_shipment_cost", th.NumberType),
        th.Property("tsv", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("untaxed_amount", th.NumberType),
        th.Property("untaxed_amount_cache", th.NumberType),
        th.Property("untaxed_amount_cpny_ccy_cache", th.NumberType),
        th.Property("warehouse", th.IntegerType),
        th.Property("weight", th.NumberType),
        th.Property("weight_digits", th.IntegerType),
        th.Property("weight_uom", th.IntegerType),
        th.Property("weight_uom_symbol", th.StringType),
        th.Property("write_date", th.DateTimeType),
        th.Property("write_uid", th.IntegerType),
    ).to_dict()

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        return 


class PurchaseOrders(FulFilStream):
    name = "purchase_orders"
    path = "/purchase.purchase"
    primary_keys = ["id"]
    replication_filter = {"filter": "updated_at_min", "stream": "purchase_order_individual"}

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("rec_name", th.StringType)
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "purchase_order_id": record["id"],
        }


class PurchaseOrder(FulFilStreamChildStream):
    name = "purchase_order_individual"
    path = "/purchase.purchase/{purchase_order_id}"
    primary_keys = ["purchase_order_id"]
    parent_stream_type = PurchaseOrders
    replication_key = "write_date"

    schema = th.PropertiesList(
        th.Property("acknowledgement_status", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("attachments", th.CustomType({"type": ["array", "string"]})),
        th.Property("comment", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("company", th.IntegerType),
        th.Property("create_date", th.DateTimeType),
        th.Property("create_uid", th.IntegerType),
        th.Property("currency", th.IntegerType),
        th.Property("currency_digits", th.IntegerType),
        th.Property("currency_symbol", th.StringType),
        th.Property("customer", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("delivery_address", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("delivery_date", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("description", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("drop_location", th.IntegerType),
        th.Property("drop_shipments", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("id", th.IntegerType),
        th.Property("incoterm", th.StringType),
        th.Property("invoice_address", th.IntegerType),
        th.Property("invoice_method", th.StringType),
        th.Property("invoice_state", th.StringType),
        th.Property("invoices", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("invoices_ignored", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("invoices_recreated", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("last_modification", th.DateTimeType),
        th.Property("lines", th.ArrayType(th.IntegerType)),
        th.Property("messages", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("metadata", th.CustomType({"type": ["object", "string"]})),
        th.Property("metafields", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("moves", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("number", th.StringType),
        th.Property("party", th.IntegerType),
        th.Property("party_lang", th.StringType),
        th.Property("payment_term", th.IntegerType),
        th.Property("payments", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("private_notes", th.CustomType({"type": ["array", "string"]})),
        th.Property("public_notes", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("purchase_date", th.DateType),
        th.Property("purchase_person", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("purchase_requests", th.ArrayType(th.IntegerType)),
        th.Property("rec_blurb", rec_blurb),
        th.Property("rec_name", th.StringType),
        th.Property("reference", th.StringType),
        th.Property("requested_delivery_date", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("requested_shipping_date", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("ship_from_address", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("shipment_method", th.StringType),
        th.Property("shipment_returns", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("shipment_state", th.StringType),
        th.Property("shipments", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("state", th.StringType),
        th.Property("tax_amount", th.NumberType),
        th.Property("tax_amount_cache", th.NumberType),
        th.Property("total_amount", th.NumberType),
        th.Property("total_amount_cache", th.NumberType),
        th.Property("total_quantity", th.NumberType),
        th.Property("transportation_mode", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("tsv", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("untaxed_amount", th.NumberType),
        th.Property("untaxed_amount_cache", th.NumberType),
        th.Property("warehouse", th.IntegerType),
        th.Property("write_date", th.DateTimeType),
        th.Property("write_uid", th.IntegerType),
    ).to_dict()

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        return 


class Currencies(FulFilStream):
    name = "currencies"
    path = "/currency.currency"
    primary_keys = ["id"]
    replication_filter = {"filter": "updated_at_min", "stream": "currency"}

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("rec_name", th.StringType)
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "currency_id": record["id"],
        }


class Currency(FulFilStreamChildStream):
    name = "currency"
    path = "/currency.currency/{currency_id}"
    primary_keys = ["currency_id"]
    parent_stream_type = Currencies
    replication_key = "write_date"

    schema = th.PropertiesList( 
        th.Property("active", th.BooleanType),
        th.Property("attachments", th.CustomType({"type": ["array", "string"]})),
        th.Property("automatically_update_exchange_rate", th.CustomType({"type": ["string", "integer", "object", "boolean"]})),
        th.Property("code", th.StringType),
        th.Property("create_date", th.DateTimeType),
        th.Property("create_uid", th.IntegerType),
        th.Property("digits", th.IntegerType),
        th.Property("id", th.IntegerType),
        th.Property("messages", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("metadata", th.CustomType({"type": ["object", "string"]})),
        th.Property("metafields", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("name", th.StringType),
        th.Property("numeric_code", th.StringType),
        th.Property("private_notes", th.CustomType({"type": ["array", "string"]})),
        th.Property("public_notes", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("rate", th.NumberType),
        th.Property("rates", th.ArrayType(th.IntegerType)),
        th.Property("rec_blurb", rec_blurb),
        th.Property("rec_name", th.StringType),
        th.Property("rounding", th.NumberType),
        th.Property("symbol", th.StringType),
        th.Property("write_date", th.DateTimeType),
        th.Property("write_uid", th.IntegerType),
    ).to_dict()

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        return 


class SaleOrderLines(FulFilStream):
    name = "sales_order_lines"
    path = "/sale.line"
    primary_keys = ["id"]
    replication_filter = {"filter": "updated_at_min", "stream": "sales_order_lines_individual"}

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("rec_name", th.StringType)
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "sale_order_line_id": record["id"],
        }


class SalesOrderLines(FulFilStreamChildStream):
    name = "sales_order_lines_individual"
    path = "/sale.line/{sale_order_line_id}"
    primary_keys = ["id"]
    parent_stream_type = SaleOrderLines
    replication_key = "write_date"

    schema = th.PropertiesList(
        th.Property("allow_open_amount", th.BooleanType),
        th.Property("amount", th.NumberType),
        th.Property("amount_blurb", th.ObjectType(
            th.Property("subtitle", th.ArrayType(
                th.ArrayType(th.StringType)
            )),
            th.Property("title", th.StringType)
        )),
        th.Property("attachments", th.CustomType({"type": ["array", "string"]})),
        th.Property("available_carrier_services", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("available_stock_qty", th.NumberType),
        th.Property("carrier", th.CustomType({"type": ["integer", "string", "object"]})),
        th.Property("carrier_service", th.CustomType({"type": ["integer", "string", "object"]})),
        th.Property("channel_identifier", th.StringType),
        th.Property("children", th.ArrayType(th.CustomType({"type": ["integer", "string", "object"]}))),
        th.Property("cost_price_cpny_ccy", th.NumberType),
        th.Property("cost_price_cpny_ccy_cache", th.NumberType),
        th.Property("create_date", th.DateTimeType),
        th.Property("create_uid", th.IntegerType),
        th.Property("cross_dock_location", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("delivery_address", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("delivery_address_used", th.IntegerType),
        th.Property("delivery_date", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("delivery_mode", th.StringType),
        th.Property("description", th.StringType),
        th.Property("discount", th.NumberType),
        th.Property("edi_attributes", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("from_location", th.IntegerType),
        th.Property("gc_price", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("gift_card_delivery_mode", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("gift_cards", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("gift_message", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("gross_profit_cpny_ccy", th.NumberType),
        th.Property("gross_profit_cpny_ccy_cache", th.NumberType),
        th.Property("id", th.IntegerType),
        th.Property("invoice_lines", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("is_channel_shipping", th.BooleanType),
        th.Property("is_gift_card", th.BooleanType),
        th.Property("is_promotion_line", th.BooleanType),
        th.Property("is_return", th.BooleanType),
        th.Property("is_round_off", th.BooleanType),
        th.Property("item_blurb", th.ObjectType(
            th.Property("description", th.StringType),
            th.Property("subtitle", th.ArrayType(
                th.ArrayType(th.StringType)
            )),
            th.Property("title", th.StringType)
        )),
        th.Property("list_price", th.NumberType),
        th.Property("listing_sku", th.StringType),
        th.Property("message", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("messages",th.ArrayType(th.StringType)),
        th.Property("metadata", th.CustomType({"type": ["object", "string"]})),
        th.Property("metafields", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("move_canceled", th.BooleanType),
        th.Property("move_done", th.BooleanType),
        th.Property("move_exception", th.BooleanType),
        th.Property("moves", th.ArrayType(th.CustomType({"type": ["string", "integer"]}))),
        th.Property("moves_ignored", th.ArrayType(th.CustomType({"type": ["string", "integer"]}))),
        th.Property("moves_recreated", th.ArrayType(th.CustomType({"type": ["string", "integer"]}))),
        th.Property("note", th.CustomType({"type": ["string", "integer"]})),
        th.Property("on_hand_stock_qty", th.NumberType),
        th.Property("options", th.ArrayType(th.CustomType({"type": ["string", "integer"]}))),
        th.Property("order_number", th.StringType),
        th.Property("origin", th.CustomType({"type": ["string", "integer"]})),
        th.Property("override_tax_rate", th.CustomType({"type": ["string", "integer"]})),
        th.Property("parent", th.CustomType({"type": ["string", "integer"]})),
        th.Property("party", th.IntegerType),
        th.Property("price_blurb", th.CustomType({"type": ["object"]})),
        th.Property("private_notes", th.CustomType({"type": ["array", "string"]})),
        th.Property("product", th.IntegerType),
        th.Property("product_type", th.StringType),
        th.Property("product_uom_category", th.IntegerType),
        th.Property("productions", th.ArrayType(th.CustomType({"type": ["string", "integer"]}))),
        th.Property("public_notes", th.ArrayType(th.CustomType({"type": ["string", "integer"]}))),
        th.Property("purchase_request", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("purchase_request_state", th.StringType),
        th.Property("quantity", th.NumberType),
        th.Property("quantity_blurb", th.ObjectType(
            th.Property("description", th.StringType),
            th.Property("subtitle", th.ArrayType(
                th.ArrayType(th.CustomType({"type": ["string", "integer", "number"]}))
            )),
            th.Property("title", th.StringType)
        )),
        th.Property("quantity_buildable", th.NumberType),
        th.Property("quantity_canceled", th.NumberType),
        th.Property("quantity_invoiced", th.NumberType),
        th.Property("quantity_reserved", th.NumberType),
        th.Property("quantity_shipped", th.NumberType),
        th.Property("rec_blurb", rec_blurb),
        th.Property("rec_name", th.StringType),
        th.Property("recipient_email", th.StringType),
        th.Property("recipient_name", th.StringType),
        th.Property("return_reason", th.CustomType({"type": ["string", "integer"]})),
        th.Property("sale", th.IntegerType),
        th.Property("sale_date", th.DateType),
        th.Property("sale_state", th.StringType),
        th.Property("sequence", th.IntegerType),
        th.Property("shipment_cost", th.NumberType),
        th.Property("shipping_date", th.DateType),
        th.Property("shipping_time", th.CustomType({"type": ["string", "integer"]})),
        th.Property("supplier", th.CustomType({"type": ["string", "integer"]})),
        th.Property("tax_lines", th.ArrayType(th.IntegerType)),
        th.Property("taxes", th.ArrayType(th.IntegerType)),
        th.Property("to_location", th.IntegerType),
        th.Property("type", th.StringType),
        th.Property("unit", th.IntegerType),
        th.Property("unit_digits", th.IntegerType),
        th.Property("unit_price", th.NumberType),
        th.Property("untaxed_amount_cpny_ccy", th.NumberType),
        th.Property("untaxed_amount_cpny_ccy_cache", th.NumberType),
        th.Property("warehouse", th.IntegerType),
        th.Property("write_date", th.DateTimeType),
        th.Property("write_uid", th.IntegerType),
    ).to_dict()


class SupplierMeta(FulFilStream):
    name = "supplier_meta"
    path = "/purchase.product_supplier"
    primary_keys = ["id"]
    replication_filter = {"filter": "updated_at_min", "stream": "suppliers"}

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("rec_name", th.StringType)
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "supplier_id": record["id"],
        }

class Supplier(FulFilStreamChildStream):
    name = "suppliers"
    path = "/purchase.product_supplier/{supplier_id}"
    primary_keys = ["id"]
    parent_stream_type = SupplierMeta
    replication_key = "write_date"

    schema = th.PropertiesList(
        th.Property("active", th.BooleanType), 
        th.Property("attachments", th.CustomType({"type": ["array", "string"]})),
        th.Property("code", th.StringType),
        th.Property("company", th.IntegerType),
        th.Property("create_date", th.DateTimeType),
        th.Property("create_uid", th.IntegerType),
        th.Property("currency", th.IntegerType),
        th.Property("default_unit_digits", th.IntegerType),
        th.Property("draft_purchase_line", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("drop_shipment", th.BooleanType),
        th.Property("drop_shipment_available", th.BooleanType),
        th.Property("forecast_quantity_on_supply_date", th.NumberType),
        th.Property("forecast_quantity_on_supply_date_calculation", th.ArrayType(th.CustomType({"type": ["object"]}))),
        th.Property("id", th.IntegerType),
        th.Property("lead_time", th.NumberType),
        th.Property("lead_time_in_days", th.IntegerType),
        th.Property("max_unit_price", th.NumberType),
        th.Property("messages", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("metadata", th.CustomType({"type": ["object", "string"]})),
        th.Property("metafields", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("min_unit_price", th.NumberType),
        th.Property("moq", th.CustomType({"type": ["string", "integer", "object", "number"]})),
        th.Property("name", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("party", th.IntegerType),
        th.Property("prices", th.ArrayType(th.IntegerType)),
        th.Property("private_notes", th.CustomType({"type": ["array", "string"]})),
        th.Property("product", th.IntegerType),
        th.Property("public_notes", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("purchase_unit_digits", th.IntegerType),
        th.Property("purchase_uom_symbol", th.StringType),
        th.Property("quantity_available", th.CustomType({"type": ["string", "integer", "object", "number"]})),
        th.Property("quantity_available_levels", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("quantity_available_updated", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("quantity_tracking", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("rec_blurb", rec_blurb),
        th.Property("rec_name", th.StringType),
        th.Property("sequence", th.IntegerType),
        th.Property("suggested_quantity", th.NumberType),
        th.Property("suggested_quantity_formula", th.StringType),
        th.Property("suggested_quantity_purchase_uom", th.NumberType),
        th.Property("write_date", th.DateTimeType),
        th.Property("write_uid", th.IntegerType),
    ).to_dict()


class CategoryMeta(FulFilStream):
    name = "category_meta"
    path = "/product.category"
    primary_keys = ["id"]
    replication_filter = {"filter": "updated_at_min", "stream": "categories"}

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("rec_name", th.StringType)
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "category_id": record["id"],
        }

        
class Categories(FulFilStreamChildStream):
    name = "categories"
    path = "/product.category/{category_id}"
    parent_stream_type = CategoryMeta
    replication_key = "write_date"

    schema = th.PropertiesList(
        th.Property("account_cogs", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("account_expense", th.IntegerType),
        th.Property("account_parent", th.BooleanType),
        th.Property("account_revenue", th.IntegerType),
        th.Property("account_stock", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("account_stock_customer", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("account_stock_lost_found", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("account_stock_production", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("account_stock_supplier", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("accounting", th.BooleanType),
        th.Property("accounts", th.ArrayType(th.IntegerType)),
        th.Property("active", th.BooleanType),
        th.Property("attachments", th.CustomType({"type": ["array", "string"]})),
        th.Property("childs", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("create_date", th.DateTimeType),
        th.Property("create_uid", th.IntegerType),
        th.Property("customer_taxes", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("customer_taxes_used", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("google_product_category", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("id", th.IntegerType),
        th.Property("messages", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("metadata", th.CustomType({"type": ["object", "string"]})),
        th.Property("metafields", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("name", th.StringType),
        th.Property("parent", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("private_notes", th.CustomType({"type": ["array", "string"]})),
        th.Property("public_notes", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("rec_blurb", rec_blurb),
        th.Property("rec_name", th.StringType),
        th.Property("supplier_taxes", th.ArrayType(th.IntegerType)),
        th.Property("supplier_taxes_used", th.ArrayType(th.IntegerType)),
        th.Property("tax_code_avalara", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("tax_code_taxjar", th.CustomType({"type": ["string", "integer", "object"]})),
        th.Property("taxes_parent", th.BooleanType),
        th.Property("templates", th.ArrayType(th.IntegerType)),
        th.Property("write_date", th.DateTimeType),
        th.Property("write_uid", th.IntegerType),
    ).to_dict()


class ContactsListStream(FulFilStream):
    name = "contacts_list"
    path = "/party.party"
    primary_keys = ["id"]
    replication_filter = {"filter": "updated_at_min", "stream": "contacts"}

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("rec_name", th.StringType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "contact_id": record["id"],
        }

class ContactsStream(FulFilStreamChildStream):
    name = "contacts"
    path = "/party.party/{contact_id}"
    primary_keys = ["id"]
    parent_stream_type = ContactsListStream
    replication_key = "write_date"

    schema = th.PropertiesList(
        th.Property("accepts_marketing", th.BooleanType),
        th.Property("account_manager", th.BooleanType),
        th.Property("account_payable", th.BooleanType),
        th.Property("account_receivable", th.BooleanType),
        th.Property("accounts", th.ArrayType(th.IntegerType)),
        th.Property("active", th.BooleanType),
        th.Property("addresses", th.ArrayType(th.IntegerType)),
        th.Property("attachments", th.CustomType({"type": ["array", "string"]})),
        th.Property("average_purchase_order_value", th.CustomType({"type": ["string", "number", "object"]})),
        th.Property("average_sale_order_value", th.CustomType({"type": ["string", "number", "object"]})),
        th.Property("categories", th.ArrayType(th.IntegerType)),
        th.Property("channel_listings", th.ArrayType(th.IntegerType)),
        th.Property("city_state_pairs", th.StringType),
        th.Property("code", th.StringType),
        th.Property("code_readonly", th.BooleanType),
        th.Property("company", th.StringType),
        th.Property("company_currency_symbol", th.StringType),
        th.Property("contact_mechanisms", th.ArrayType(th.IntegerType)),
        th.Property("create_date", th.DateTimeType),
        th.Property("credit_amount", th.NumberType),
        th.Property("credit_available", th.NumberType),
        th.Property("credit_limit_amount", th.NumberType),
        th.Property("credit_limit_amounts", th.ArrayType(th.NumberType)),
        th.Property("credit_limit_digits", th.IntegerType),
        th.Property("currency_digits", th.IntegerType),
        th.Property("customer_location", th.IntegerType),
        th.Property("customer_payment_term", th.IntegerType),
        th.Property("customer_tax_rule", th.IntegerType),
        th.Property("default_payment_profile", th.IntegerType),
        th.Property("duns_number", th.StringType),
        th.Property("email", th.StringType),
        th.Property("fax", th.StringType),
        th.Property("full_name", th.StringType),
        th.Property("identifiers", th.ArrayType(th.IntegerType)),
        th.Property("incoterm", th.StringType),
        th.Property("integrations", th.ArrayType(th.IntegerType)),
        th.Property("is_customer", th.BooleanType),
        th.Property("is_supplier", th.BooleanType),
        th.Property("lang", th.StringType),
        th.Property("is_customer", th.BooleanType),
        th.Property("is_supplier", th.BooleanType),
        th.Property("lang", th.IntegerType),
        th.Property("langs", th.ArrayType(th.IntegerType)),
        th.Property("locations", th.ArrayType(th.IntegerType)),
        th.Property("messages", th.ArrayType(th.IntegerType)),
        th.Property("metafields", th.ArrayType(th.IntegerType)),
        th.Property("mobile", th.StringType),
        th.Property("name", th.StringType),
        th.Property("nereid_users", th.ArrayType(th.IntegerType)),
        th.Property("open_purchase_order_count", th.IntegerType),
        th.Property("open_sale_order_count", th.IntegerType),
        th.Property("party_shipping_accounts", th.ArrayType(th.IntegerType)),
        th.Property("payable", th.NumberType),
        th.Property("payable_today", th.NumberType),
        th.Property("payment_profiles", th.ArrayType(th.IntegerType)),
        th.Property("payment_terms", th.ArrayType(th.IntegerType)),
        th.Property("phone", th.StringType),
        th.Property("private_notes", th.CustomType({"type": ["array", "string"]})),
        th.Property("product_suppliers", th.ArrayType(th.IntegerType)),
        th.Property("public_notes", th.ArrayType(th.IntegerType)),
        th.Property("purchase_order_count", th.IntegerType),
        th.Property("purchase_order_frequency", th.NumberType),
        th.Property("rec_blurb", rec_blurb),
        th.Property("rec_name", th.StringType),
        th.Property("receivable", th.NumberType),
        th.Property("receivable_today", th.NumberType),
        th.Property("return_order_count", th.IntegerType),
        th.Property("sale_order_count", th.IntegerType),
        th.Property("sale_order_frequency", th.NumberType),
        th.Property("sale_price_list", th.IntegerType),
        th.Property("sale_price_lists", th.ArrayType(th.IntegerType)),
        th.Property("sale_shipment_grouping_method", th.StringType),
        th.Property("shipping_zones", th.ArrayType(th.IntegerType)),
        th.Property("supplier_location", th.IntegerType),
        th.Property("supplier_payment_term", th.IntegerType),
        th.Property("total_purchase_order_value", th.CustomType({"type": ["string", "number", "object"]})),
        th.Property("total_returns_value", th.CustomType({"type": ["string", "number", "object"]})),
        th.Property("total_sale_order_value", th.CustomType({"type": ["string", "number", "object"]})),
        th.Property("unbilled_amount", th.NumberType),
        th.Property("website", th.StringType),
        th.Property("write_date", th.DateTimeType),
        th.Property("write_uid", th.IntegerType)
    ).to_dict()

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        return


class PurchaseOrderLines(FulFilStream):
    name = "purchase_order_lines"
    path = "/purchase.line"
    primary_keys = ["id"]
    replication_filter = {"filter": "updated_at_min", "stream": "purchase_order_lines_individual"}

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("rec_name", th.StringType)
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "purchase_order_line_id": record["id"],
        }


class PurchaseOrdersLines(FulFilStreamChildStream):
    name = "purchase_order_lines_individual"
    path = "/purchase.line/{purchase_order_line_id}"
    primary_keys = ["id"]
    parent_stream_type = PurchaseOrderLines
    replication_key = "write_date"

    schema = th.PropertiesList(
        th.Property("amount", th.NumberType),
        th.Property("amount_blurb", th.CustomType({"type": ["object", "string"]})),
        th.Property("attachments", th.CustomType({"type": ["array", "string"]})),
        th.Property("create_date", th.DateTimeType),
        th.Property("create_uid", th.IntegerType),
        th.Property("delivery_date", th.DateType),
        th.Property("delivery_date_edit", th.BooleanType),
        th.Property("delivery_date_store", th.BooleanType),
        th.Property("description", th.StringType),
        th.Property("from_location", th.IntegerType),
        th.Property("id", th.IntegerType),
        th.Property("invoice_lines", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("item_blurb", th.ObjectType(
            th.Property("description", th.StringType),
            th.Property("subtitle", th.ArrayType(
                th.ArrayType(th.StringType)
            )),
            th.Property("title", th.StringType)
        )),
        th.Property("messages",th.ArrayType(th.StringType)),
        th.Property("messages",th.ArrayType(th.StringType)),
        th.Property("metadata", th.CustomType({"type": ["object", "string"]})),
        th.Property("metafields", th.ArrayType(th.CustomType({"type": ["string", "integer", "object"]}))),
        th.Property("move_canceled", th.BooleanType),
        th.Property("move_done", th.BooleanType),
        th.Property("move_exception", th.BooleanType),
        th.Property("moves", th.ArrayType(th.CustomType({"type": ["string", "integer"]}))),
        th.Property("moves_ignored", th.ArrayType(th.CustomType({"type": ["string", "integer"]}))),
        th.Property("moves_recreated", th.ArrayType(th.CustomType({"type": ["string", "integer"]}))),
        th.Property("note", th.CustomType({"type": ["string", "integer"]})),
        th.Property("price_blurb", th.CustomType({"type": ["object"]})), #TODO:
        th.Property("private_notes", th.CustomType({"type": ["array", "string"]})),
        th.Property("product", th.IntegerType),
        th.Property("product_supplier", th.CustomType({"type": ["string", "integer"]})),
        th.Property("product_uom_category", th.IntegerType),
        th.Property("production", th.CustomType({"type": ["string", "integer"]})), #TODO:
        th.Property("public_notes", th.ArrayType(th.CustomType({"type": ["string", "integer"]}))),
        th.Property("purchase", th.IntegerType),
        th.Property("purchase_state", th.StringType),
        th.Property("quantity", th.NumberType),
        th.Property("quantity_blurb", th.ObjectType(
            th.Property("description", th.StringType),
            th.Property("subtitle", th.ArrayType(
                th.ArrayType(th.CustomType({"type": ["string", "integer", "number"]}))
            )),
            th.Property("title", th.StringType)
        )),
        th.Property("quantity_canceled", th.NumberType),
        th.Property("quantity_invoiced", th.NumberType),
        th.Property("quantity_reserved", th.NumberType),
        th.Property("rec_blurb", th.ObjectType(
            th.Property("description", th.StringType),
            th.Property("id", th.IntegerType),
            th.Property("subtitle", th.ArrayType(
                th.ArrayType(th.CustomType({"type": ["string", "integer", "number"]}))
            )),
            th.Property("title", th.StringType)
        )),        
        th.Property("rec_name", th.StringType),
        th.Property("requested_ship_date",th.DateType),
        th.Property("requests", th.CustomType({"type": ["array", "string"]})),
        th.Property("requested_ship_date", th.DateType),
        th.Property("sequence", th.IntegerType),
        th.Property("supplier_product_code", th.StringType),
        th.Property("supplier_product_name", th.StringType),
        th.Property("taxes", th.ArrayType(th.IntegerType)),
        th.Property("to_location", th.IntegerType),
        th.Property("type", th.StringType),
        th.Property("unit", th.IntegerType),
        th.Property("unit_digits", th.IntegerType),
        th.Property("unit_price", th.NumberType),
        th.Property("work", th.CustomType({"type": ["string", "integer", "number"]})),
        th.Property("write_date", th.DateTimeType),
        th.Property("write_uid", th.IntegerType),
    ).to_dict()