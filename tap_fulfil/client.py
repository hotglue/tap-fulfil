"""REST client handling, including FulFilStream base class."""
import logging
import urllib.parse
from typing import Any, Callable, Dict, Optional, List
import copy
import singer
from singer import StateMessage
import pendulum
from datetime import datetime
from backports.cached_property import cached_property

import backoff
import requests
from singer_sdk.authenticators import APIKeyAuthenticator
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError
from singer_sdk.streams import RESTStream
from singer_sdk.helpers.jsonpath import extract_jsonpath

logging.getLogger("backoff").setLevel(logging.CRITICAL)


class FulFilStream(RESTStream):
    """FulFil stream class."""

    records_jsonpath = "$[*]"
    replication_filter = None
    _page_size = 200

    @property
    def url_base(self) -> str:
        """Return the API URL root, configurable via tap settings."""
        return f"https://{self.config['instance']}.fulfil.io/api/v2/model"

    @property
    def authenticator(self) -> APIKeyAuthenticator:
        """Return a new authenticator object."""
        return APIKeyAuthenticator.create_for_stream(
            self,
            key="X-API-KEY",
            value=self.config.get("api_key"),
            location="header"
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        data = response.json()
        if data and type(data) == list:
            parsed = urllib.parse.urlparse(response.url)
            page = urllib.parse.parse_qs(parsed.query).get("page")
            if page:
                next_page_token = int(page[0]) + 1
                return next_page_token
        return None

    @cached_property
    def child_replication_key(self):
        rep_key_stream = self.replication_filter.get("stream")
        if self.tap_state.get("bookmarks"):
            rep_keys = self.tap_state["bookmarks"].get(rep_key_stream)
            if rep_keys and "partitions" in rep_keys.keys():
                rep_keys = rep_keys["partitions"]
                if rep_keys:
                    date_str = rep_keys[0].get("replication_key_value")
                    if date_str:
                       return pendulum.parse(date_str)
        return None

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        params["per_page"] = self._page_size 
        params["page"] = (next_page_token or 1)
        
        if self.replication_filter:
            start_date = self.config.get("start_date")
            start_date = pendulum.parse(start_date)
            rep_key_filter = self.replication_filter.get("filter")
            rep_key = self.child_replication_key
            start_date = (rep_key or start_date)
            start_date = start_date.replace(tzinfo=None)
            if start_date>datetime(2015,1,1) or rep_key:
                start_date = start_date.strftime("%Y-%m-%dT%H:%M:%S")
                params[rep_key_filter] = start_date
        return params

    def parse_response(self, response: requests.Response):
        """Parse the response and return an iterator of result rows."""
        input = response.json()
        if isinstance(input, dict) and response.status_code in [400, 500]:
            yield
        else:
            yield from extract_jsonpath(self.records_jsonpath, input=input)


    def validate_response(self, response: requests.Response) -> None:
        """Validate HTTP response."""
        
        if response.status_code==500:
            if response.json().get("description") == "There was an error processing this request":
                return
        
        if response.status_code==400:
            message = response.json().get("message")
            logging.info(f"Ignoring record: {message}")
            return

        if 400 < response.status_code < 600:
            msg = (
                f"{response.status_code} Server Error: "
                f"{response.reason} for path: {self.path}"
            )
            raise RetriableAPIError(msg)
    
    def request_decorator(self, func: Callable) -> Callable:
        """Instantiate a decorator for handling request failures."""
        decorator: Callable = backoff.on_exception(
            backoff.expo,
            (
                RetriableAPIError,
                requests.exceptions.ReadTimeout,
                requests.exceptions.ConnectionError
            ),
            max_tries=10,
            factor=2,
        )(func)
        return decorator

    def convert_class(self, row):
        if isinstance(row, dict) and "__class__" in row.keys():
            if row["__class__"] == "datetime":
                return row["iso_string"].split(".")[0]
            elif row["__class__"] == "date":
                return row["iso_string"]
            elif row["__class__"] == "Decimal":
                return float(row["decimal"])
            elif row["__class__"] == "timedelta":
                return float(row["seconds"])
            return row
        elif isinstance(row, dict):
            for k, v in row.items():
                row[k] = self.convert_class(v)
        elif isinstance(row, list):
            row = [self.convert_class(r) for r in row]
        return row

    def post_process(self, row: dict, context: Optional[dict] = None) -> Optional[dict]:
        return self.convert_class(row)
    
    def _sync_children(self, child_context: dict) -> None:
        for child_stream in self.child_streams:
            if child_stream.selected or child_stream.has_selected_descendents:
                child_stream.sync_custom(context=child_context)


class FulFilStreamChildStream(FulFilStream):

    schema_writed = False

    def _sync_records(  # noqa C901  # too complex
        self, context: Optional[dict] = None
    ) -> None:
        record_count = 0
        current_context: Optional[dict]
        context_list: Optional[List[dict]]
        context_list = [context] if context is not None else self.partitions
        selected = self.selected

        for current_context in context_list or [{}]:
            partition_record_count = 0
            current_context = current_context or None
            state_partition_context = self._get_state_partition_context(current_context)
            self._write_starting_replication_value(current_context)
            child_context: Optional[dict] = (
                None if current_context is None else copy.copy(current_context)
            )
            for record_result in self.get_records(current_context):
                if isinstance(record_result, tuple):
                    # Tuple items should be the record and the child context
                    record, child_context = record_result
                else:
                    record = record_result
                child_context = copy.copy(
                    self.get_child_context(record=record, context=child_context)
                )
                for key, val in (state_partition_context or {}).items():
                    # Add state context to records if not already present
                    if key not in record:
                        record[key] = val

                # Sync children, except when primary mapper filters out the record
                if self.stream_maps[0].get_filter_result(record):
                    self._sync_children(child_context)
                self._check_max_record_limit(record_count)
                if selected:
                    if (record_count - 1) % self.STATE_MSG_FREQUENCY == 0:
                        self._write_state_message()
                    self._write_record_message(record)
                record_count += 1
                partition_record_count += 1
                self._increment_stream_state(record, context=current_context)
        parts = self.tap_state["bookmarks"][self.name]["partitions"]
        for p in parts:
            if p.get("progress_markers"):
                p.update(p["progress_markers"])
                del p["progress_markers"]
            if p.get("Note"):
                del p["Note"]

        parts = [p for p in parts if p.get("replication_key_value")]
        if parts:
            max_part = max(parts, key=lambda x: list(x["replication_key_value"]))
            self.tap_state["bookmarks"][self.name]["partitions"] = [max_part]
            singer.write_message(StateMessage(value=self.tap_state))
    
    def sync_custom(self, context: Optional[dict] = None) -> None:
        msg = f"Beginning {self.replication_method.lower()} sync of '{self.name}'"
        if context:
            msg += f" with context: {context}"
        self.logger.info(f"{msg}...")

        # Use a replication signpost, if available
        signpost = self.get_replication_key_signpost(context)
        if signpost:
            self._write_replication_key_signpost(context, signpost)

        # Send a SCHEMA message to the downstream target:
        if not self.schema_writed:
            self._write_schema_message()
            self.schema_writed = True
        # Sync the records themselves:
        self._sync_records(context)
