"""FulFil tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_fulfil.streams import (
    Categories,
    CategoryMeta,
    ContactsListStream,
    ContactsStream,
    Currencies,
    Currency,
    ProductsStream,
    ProductStream,
    PurchaseOrder,
    PurchaseOrders,
    SaleOrderLines,
    SalesOrder,
    SalesOrderLines,
    SalesOrders,
    StockLocationsStream,
    StockLocationStream,
    Supplier,
    SupplierMeta,
    ProductSuppliersStream,
    ProductSupplierStream,
    PurchaseOrderLines,
    PurchaseOrdersLines
)

STREAM_TYPES = [
    ProductsStream,
    ProductStream,
    StockLocationsStream,
    StockLocationStream,
    SalesOrders,
    SalesOrder,
    PurchaseOrders,
    PurchaseOrder,
    Currencies,
    Currency,
    SaleOrderLines,
    SalesOrderLines,
    SupplierMeta,
    Supplier,
    CategoryMeta,
    Categories,
    ContactsListStream,
    ContactsStream,
    ProductSuppliersStream,
    ProductSupplierStream,
    PurchaseOrderLines,
    PurchaseOrdersLines
]


class TapFulFil(Tap):
    """FulFil tap class."""
    name = "tap-fulfil"

    def __init__(
        self,
        config=None,
        catalog=None,
        state=None,
        parse_env_config=False,
        validate_config=True,
    ) -> None:
        self.config_file = config[0]
        super().__init__(config, catalog, state, parse_env_config, validate_config)

    config_jsonschema = th.PropertiesList(
        th.Property(
            "api_key",
            th.StringType,
            required=True,
            description="The token to authenticate against the API service"
        ),
        th.Property(
            "instance",
            th.StringType,
            required=True
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync"
        )
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapFulFil.cli()  